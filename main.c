#include <stdio.h>
#include <stdlib.h>


void check_prime(int n) {
    float half = n/2 ;
    for(int j=2; j<=half; j++) {
        if(n%j == 0) {
            return;
        }
    }
    printf("%f\n", n);
}

void swap_ints(int *a, int *b) {
    if (NULL == *a || NULL == *b) {
        printf("Unable to swap.\n");
        return;
    }
    int temp = *a;
    *a = *b;
    *b = temp;
}


int main(int argc, char** argv )
{
    if(argc != 3) {
        printf("Please provide 2 integer inputs.\n");
        exit(0);
    }
    int start = strtol(argv[1], NULL, 10);
    int end = strtol(argv[2], NULL, 10);
    if (end < 0)
        end *= -1;
    if (start < 0)
        start *= -1;
    if (end < start)
        swap_ints(&start, &end);
    for (int i = start; i <= end; i++) {
        check_prime(i);
    }
    return 0;
}
